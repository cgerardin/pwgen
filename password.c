#include <stdlib.h>

char* pwgen(int length) {

    if (length < 1) {
        return NULL;
    }

    char* password = malloc(length + 1 * sizeof (char));

    for (int i = 0; i < length; i++) {

        switch (rand() % 3) {

            case 0:
                password[i] = 48 + (rand() % 10); // Chiffres
                break;
            case 1:
                password[i] = 65 + (rand() % 26); // Majuscules
                break;
            case 2:
                password[i] = 97 + (rand() % 26); // Minuscules

        }

    }
    password[length] = '\0';

    return password;

}
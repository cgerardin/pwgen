#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "main.h"
#include "password.h"

int main(int argc, char** argv) {

    int length = DEFAULT_LENGTH;

    if (argc == 2) {

        length = atoi(argv[1]);

        if (length < MIN_LENGTH || length > MAX_LENGTH) {
            printf("La longueur souhaitee doit etre comprise entre %d et %d caracteres.\n", MIN_LENGTH, MAX_LENGTH);
            return (EXIT_SUCCESS);
        }

    }

    srand(time(NULL));
    
    char* p = pwgen(length);
    printf("%s\n", p);
    free(p);

    return (EXIT_SUCCESS);
}


#ifndef PASSWORD_H
#define PASSWORD_H

/*
 * Génère un mot de passe de longueur 'length'
 * Renvoie un pointeur sur la chaîne générée
 * Renvoie NULL en cas d'erreur
 */
char* pwgen(int length);

#endif /* PASSWORD_H */

